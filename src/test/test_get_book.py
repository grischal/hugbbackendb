import unittest
import app.services.book_service as BookFunctions
from app.services import book_service

""" Tests the get_book() function to make sure it is working as expected """
class testGetBook(unittest.TestCase):
    """ Sets up the test for each test method """
    def setUp(self):
        self.__book_service = book_service.BookService()
        self.books = self.__book_service.get_all_books()
    
    """ Cleans up for the next test method """
    def tearDown(self):
        pass
    
    """ Test to see if the function gives the right book when given a title that exists """
    def test_get_book_exists(self):
        fake_title = "Bible" #The good book
        fakebook = {"book_id": -1,
        "user_id": -2,
        "name": "Bible",
        "author": ["my leg"],
        "genre": ["fantasy"],
        "published": 10,
        "condition": 2,
        "status": 2}


        #most disgusting "solution" i've ever had the misfortune of making but i've yet to be sent directly to hell so i think i'm good.
        self.__book_service.change_book_availability(-1, 2)
        gottem = self.__book_service.get_book(fake_title).values()
        gottem_again = fakebook.values()

        lis1 = []
        for i in gottem:
            lis1.append(i)

        lis2 = []
        for i in gottem_again:
            lis2.append(i)

        self.assertEqual(lis1,lis2)

    """ Test to see if the function gives the right response when book doesn't exist """
    def test_get_book_not_exists(self):
        fake_title = "According to all known laws of aviation"
        self.assertEqual(self.__book_service.get_book(fake_title), "Book doesn't exist!")
    
    """ Test to see if the function gives the right response when given an invalid title """
    def test_get_book_unexpected_input(self):
        fake_title = 25
        self.assertEqual(self.__book_service.get_book(fake_title), "Book doesn't exist!")

if __name__ == "__main__":
       unittest.main()