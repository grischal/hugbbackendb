T-303-HUGB 2019 Group 8

Authors and acknowledgment

* Hávar Sigurðsson
* Daníel Aron Davíðsson
* Styrkár Blær Styrmisson
* Ólafur Andri Davíðsson
* Þór Breki
* Halldór Þorri Ásgeirsson


**Description:**  
In this school project we will make a Sharing system for people to either lend items/objects or borrow from other people. You should be able to log in and view ads from other people.

**Usage:**  
This application will be used to lend and borrow certain items/objects for a price and  duration from other people useing the application.



**Project status:**  
in progress, early stages

**How to run:**  
Install requirements with: `pip install -r requirements.txt`  
Navigate to the \src folder: `cd src`  
Then run the server with this command: `python -m app.server.server`  
You should see a nice message:
>  Starting server at:   
0.0.0.0:8080


**How to run our unit tests:**  
In the terminal you have to be loacted in our \src folder. Then write in your console `python -m test.{test_you_want_to_run}`.
