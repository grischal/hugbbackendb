from app.data import dummydata
from app.models.Rental import Rental
from app.models.Exchange import Exchange
from app.services.exchange_service import ExchangeService
from app.services.rental_service import RentalService
from app.services.book_service import BookService
import unittest

""" A test suite that tests the backend's exchange/rental operations """
class testExchangeBook(unittest.TestCase):
    """ Code that is run before every test case """
    def setUp(self):
        self.__rental_service = RentalService()
        self.__exchange_service = ExchangeService()
        self.__book_service = BookService()

    """
        Tests whether the create_rental() method actually creates and stores the
        new rental.
    """
    def test_create_rental_creates_rental(self):
        old_rentals_length = len(dummydata.rentals)
        new_rental = Rental(1, 1, 2, -1)
        self.__rental_service.create_rental(new_rental)
        new_rentals_length = len(dummydata.rentals)
        dummy_rental = self.__get_latest_rental()
        self.assertEqual(old_rentals_length + 1, new_rentals_length)
        self.assertEqual(new_rental.get_book_id(), dummy_rental.get_book_id())
        self.assertEqual(new_rental.get_lender_id(), dummy_rental.get_lender_id())
        self.assertEqual(new_rental.get_rental_id(), dummy_rental.get_rental_id())
        self.assertEqual(new_rental.get_renter_id(), dummy_rental.get_renter_id())

    """
        Checks if the create_rental() method throws a type error when given
        wrong parameter values.
    """
    def test_create_rental_throws_type_error(self):
        with self.assertRaises(TypeError):
            self.__rental_service.create_rental(None)
    
    """ 
        Tests if the create_exchange() method actually creates and stores the
        new exchange.
    """
    def test_create_exchange_creates_exchange(self):
        old_exchanges_length = len(dummydata.exchanges)
        new_exchange = Exchange(1, 2, 1, 2, -1)
        self.__exchange_service.create_exchange(new_exchange)
        new_exchanges_length = len(dummydata.exchanges)
        dummy_exchange = self.__get_latest_exchange()
        self.assertEqual(old_exchanges_length + 1, new_exchanges_length)
        self.assertEqual(new_exchange.get_bookid1(), dummy_exchange.get_bookid1())
        self.assertEqual(new_exchange.get_bookid2(), dummy_exchange.get_bookid2())
        self.assertEqual(new_exchange.get_userid1(), dummy_exchange.get_userid1())
        self.assertEqual(new_exchange.get_userid2(), dummy_exchange.get_userid2())
        self.assertEqual(new_exchange.get_exchange_id(), dummy_exchange.get_exchange_id())
    
    """
        Tests whether the create_exchange() method throws a type error when
        given wrong parameter values.
    """
    def test_create_exchange_throws_type_error(self):
        with self.assertRaises(TypeError):
            self.__exchange_service.create_exchange(None)
    
    """
        Tests whether the make_book_unavailable() method updates a book to have
        a status of 'unavailable'
    """
    def test_make_book_unavailable_makes_book_unavailable(self):
        book_id = 1
        self.__book_service.make_book_unavailable(book_id)
        book_obj = dummydata.books[book_id]
        self.assertEqual("unavailable", book_obj.get_status())
    
    """
        Tests whether the make_book_available() method updates a book to have a
        status of 'available'
    """
    def test_make_book_available_makes_book_available(self):
        # First make it unavailable
        book_id = 1
        self.__book_service.make_book_unavailable(book_id)
        # Then make it available
        self.__book_service.make_book_available(book_id)
        book_obj = dummydata.books[book_id]
        self.assertEqual("available", book_obj.get_status())

    """
        Tests whether the is_book_available() method returns True if a book is
        indeed available
    """
    def test_is_book_available_returns_true_when_available(self):
        book_id = 3
        is_available = self.__book_service.is_book_available(book_id)
        self.assertTrue(is_available)

    """
        Tests whether the is_book_available() method returns False if a book is
        not available
    """
    def test_is_book_available_returns_false_when_unavailable(self):
        # Make book unavailable
        book_id = 2
        self.__book_service.make_book_unavailable(book_id)
        is_available = self.__book_service.is_book_available(book_id)
        self.assertFalse(is_available)

    """
        A helper function for getting the latest rental in the 'rentals'
        dictionary in dummydata
    """
    def __get_latest_rental(self):
        return dummydata.rentals[dummydata.next_rental_id - 1]

    """
        A helper function for getting the latest exchange in the 'exchanges'
        dictionary in dummydata
    """
    def __get_latest_exchange(self):
        return dummydata.exchanges[dummydata.next_exchange_id - 1]

if __name__ == "__main__":
    unittest.main()
