# Server - rental functions

Here you can find all of the different rental related functionality that the file [server.py](../../../src/app/server.py) makes available to the client. Within each JSON body, you must include a `"op"` value in order to specify the operation you want the server to perform. All properties are required unless a property is specified to be optional.

## Table of Contents

1. [create_rental](#create_rental)
2. [close_rental](#close_rental)
3. [get_all_rentals](#get_all_rentals)
4. [get_rental_info](#get_rental_info)
5. [hard_delete_rental](#hard_delete_rental)
6. [decrement_next_rental_id](#decrement_next_rental_id)

---

## create_rental

### Description

Creates a new rental object according to the information given (provided that it's valid) and stores it. Once the rental is created, the function returns the new rental object (ID included).

### Example JSON body

``` json
{
    "op": "create_rental",
    "rental_model": {
        "lender_id": 2,
        "book_id": 3,
        "renter_id": 1
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| rental_model | Object | Holds all relevant information regarding the soon-to-be-created rental.
| lender_id | Number | The ID of the user who is the renter in this rental.
| book_id | Number | The ID of *lender_id*'s book that he's about to rent out.
| renter_id | Number | The ID of the user receiving *lender_id*'s book.

<br />

## close_rental

### Description

Marks the rental with the given ID as *closed*. Additional info is then provided for the system to handle appropriately. **This functionality is only accessible to the lender of a rental**. Once this message goes through, a notification is sent to the renter where he is asked to review the lender.

### Example JSON body

``` json
{
    "op": "close_rental",
    "rental_id": 2,
    "paid": true,
    "lender_review_model": {}
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| rental_id | Number | The ID of the rental to close/finish.
| paid | Boolean | `True` if the rental has been paid by the renter, `False` otherwise.
| lender_review_model | Object | The review the lender left the renter.

<br />

## get_all_rentals

### Description

Returns a dictionary of all rentals registered within the system.

### Example JSON body

``` json
{
    "op": "get_all_rentals"
}
```

### Property descriptions

There are no additional properties to describe.

<br />

## get_user_lends

### Description

Get a dictionary of all rentals of a particular users books.

### Example JSON body
 
``` json
{
    "op": "get_user_lends",
    "user_id": 4
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user to get lending information about.

<br />


## get_rental_info

### Description

Returns all the information regarding a rental that as the specified rental ID.

### Example JSON body

``` json
{
    "op": "get_rental_info",
    "rental_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| rental_id | Number | The ID of the rental to get information about.

<br />

## hard_delete_rental

> **WARNING**: This method should only be used in test files, not in production.

### Description

Instead of just marking a rental as deleted, this method *actually* deletes a rental from the system, so be careful with it.

### Example JSON body

``` json
{
    "op": "hard_delete_rental",
    "rental_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| rental_id | Number | The ID of the rental to remove, forever.

<br />

## decrement_next_rental_id

> **WARNING**: This method should only be used in test files, not in production.

### Description

Decrements the next available ID for rentals, should only be used in test files.

### Example JSON body

``` json
{
    "op": "decrement_next_rental_id"
}
```

### Property descriptions

There are no extra properties to describe.